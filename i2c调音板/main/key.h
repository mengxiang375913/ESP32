#ifndef _KEY_H
#define _KEY_H

void key_open(int mode);
uint32_t Key_read();
void Key_write(uint32_t num);
#endif