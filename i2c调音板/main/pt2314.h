#ifndef _PT2314_H
#define _PT2314_H

void pt2314_open(unsigned char channal);
void volume(unsigned char num);
void balance(unsigned char  wh,unsigned char  num);
void choosech(unsigned char ch,unsigned char gain,unsigned char loud);
void BassTreble(unsigned char jud,unsigned char  num);
unsigned char pt2314_read(unsigned char num,unsigned char mod);
void pt2314_write(unsigned char add_sub,unsigned char num,unsigned char mod);
#endif