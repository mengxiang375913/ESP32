#include <reg52.h>
#include "tea5767.h"
#include "I2C.h"
#include "delay.h"
#define	MAX_PLL	13156
#define	MIN_PLL	10654
//unsigned char radio_write_data[5]={0x29,0xc2,0x20,0x11,0x00};        //要写入TEA5767的数据
unsigned char radio_write_data[5]={0x29,0x9e,0x70,0x16,0x80};        //要写入TEA5767的数据
//unsigned char radio_write_data[5]={0x2a,0xb6,0x40,0x11,0x40};        //要写入TEA5767的数据
unsigned char radio_read_data[5];        //TEA5767读出的状态
unsigned int default_pll=0x301d;         //0x29f9;        //默认存台的pll,95.8MHz
unsigned int max_pll=0x339b;           //108MHz时的pll,
unsigned int min_pll=9000;             //70MHz时的pll
unsigned long frequency;
unsigned int pll;

//由频率计算PLL
void get_pll(void)
{
	unsigned char hlsi;
	// unsigned int twpll=0;
	hlsi=radio_write_data[2]&0x10;  //HLSI位
	if (hlsi)
		pll=(unsigned int)((float)((frequency+225)*4)/(float)32.768);    //频率单位:k
	else
		pll=(unsigned int)((float)((frequency-225)*4)/(float)32.768);    //频率单位:k
}


//由PLL计算频率
void get_frequency(void)
{
	unsigned char hlsi;
	unsigned int npll=0;
	npll=pll;
	hlsi=radio_write_data[2]&0x10;
	if (hlsi)
		frequency=(unsigned long)((float)(npll)*(float)8.192-225);    //频率单位:KHz
	else
		frequency=(unsigned long)((float)(npll)*(float)8.192+225);    //频率单位:KHz
}

void tea5767_write(){
	unsigned char i;
	IIC_Start();
	IIC_WriteByte(0xc0);        //TEA5767写地址
	if (!IIC_testack())
	{
		for (i=0;i<5;i++)
		{
			IIC_WriteByte(radio_write_data[i]);
			IIC_Ack();
		}
	}
	IIC_Stop();
}

void tea5767_read(){
	unsigned char i;
	unsigned char temp_l,temp_h;
	pll=0;
	IIC_Start();
	IIC_WriteByte(0xc1);        //TEA5767读地址
	if (!IIC_testack())
	{
		for (i=0;i<5;i++)
		{
			radio_read_data[i]=IIC_ReadByte();
			IIC_Ack();
		}
	}
	IIC_Stop();
	temp_l=radio_read_data[1];
	temp_h=radio_read_data[0];
	temp_h&=0x3f;
	pll=temp_h*256+temp_l;
	get_frequency();
}

//手动设置频率,mode=1,+0.1MHz; mode=0:-0.1MHz ,不用考虑TEA5767用于搜台的相关位:SM,SUD
void search(unsigned char mode)
{
	tea5767_read();
	if (mode == 1)
	{
		frequency+=1000;
		if (frequency>max_freq)
			frequency=min_freq;
	} 
	else if (mode==0)
	{
		frequency-=1000;
		if (frequency<min_freq)
			frequency=max_freq;
	}
	else if(mode==2)
	{
		frequency += 100;
		if (frequency>max_freq)
			frequency=min_freq;
	}
	else if(mode == 3)
	{
		frequency-=100;
		if (frequency<min_freq)
			frequency=max_freq;
	}
	else if(mode== 4 )
	{
		frequency += 10;
		if (frequency>max_freq)
			frequency=min_freq;
	}
	else if(mode == 5)
	{
		frequency-=10;
		if (frequency<min_freq)
			frequency=max_freq;
	}
	get_pll();
	radio_write_data[0]=pll/256;
	radio_write_data[1]=pll%256;
	radio_write_data[2]=0x60;
	radio_write_data[3]=0x10;
	radio_write_data[4]=0x00;
	tea5767_write();
}

void tea767_auto_search(bit mode)	  //自动搜台,mode=1,频率增加搜台; mode=0:频率减小搜台,不过这个好像不能循环搜台
{
	bit FindStation=0;
	while(!FindStation)
	{
	    if(mode)
		{
			pll+=10;	  	//加10再搜索，防止再次搜索到同一个台，即搜索不走
			if(pll>max_freq)
				pll=min_freq;
	    	//TEA5767_Write_Data[2]=0xa0;
			radio_write_data[2]=0xE0;
		}
	    else
		{
			pll-=10;	 	//减10再搜索，防止再次搜索到同一个台，即搜索不走
			if(pll<min_freq)
				pll=max_freq;
	        //radio_write_data[2]=0x20; 
			radio_write_data[2]=0x60; 
		}   
		radio_write_data[0]=(pll/256)+0xC0;			//静音搜索
	    radio_write_data[1]=pll%256;    
	    radio_write_data[3]=0x17;
	    radio_write_data[4]=0x80;
	    tea5767_write();
		DelayMs(50);				//每隔50ms读取一次TEA5767状态
	    tea5767_read();
	    while(!(radio_read_data[0]&0x80))    //RF=0,搜台还没成功
	    {
			DelayMs(50);				//每隔50ms读取一次TEA5767状态
		    tea5767_read();
	    }
		//直到RF=1,搜台成功或者到达频率极限标志 
		if(radio_read_data[0]&0x40)			//搜台到达频率极限，PLL重新设置，重新开始搜索
		{
			if(mode)
				pll=MIN_PLL;
			else
				pll=MAX_PLL;
			FindStation=0;
		}
		else	//搜索到一个电台
		{
			if(((radio_read_data[2]&0x7F)>0x31)&&((radio_read_data[2]&0x7F)<0x3E))		//判断IF计数器值(中频频率)是否在31H～ 3EH之间
			{	
				if((radio_read_data[3]&0xF0)>0xB0)						   //判断输出的电台信号是否大于1010B（最小0000B，最大1111B）
				{
					if(radio_read_data[2]&0x80)				//立体声信号
						radio_write_data[2]=0x60;				//打开立体声
					else
						radio_write_data[2]=0x68;				//关闭立体声
					radio_write_data[0]=(pll/256)&0x3F;		//LR输出有效，不在搜索状态
					radio_write_data[1]=pll%256;
					radio_write_data[3]=0x17;
					radio_write_data[4]=0x80;
					tea5767_write();							//写入，就发出声音   
					FindStation=1;
				}
				else	   //输出的电台信号太小，忽略，继续搜
				{
					FindStation=0;
				}
			}
			else	//IF计数器值是不在31H～ 3EH之间,PLL+10后继续搜索
			{
				FindStation=0;
			}

		}
	}
}


void radio_write(unsigned char *str,int value,int len)
{
	if(len==1){
		if(value<2){
			tea767_auto_search(value);
		}
	}else{
		if(value<6){
		search(value);
		}	
	}
	

}

//读TEA5767状态,并转换成频率
unsigned long radio_read(unsigned char flag)
{
	if(flag == 0){
		
	return 	frequency;
	}
	if(flag == 1){
		
		return 	(radio_read_data[3]>>4);
	}
}

void radio_open(){
	tea5767_write();
	tea5767_read();
}
