#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "PCF8574.h"
#include "delay.h"
#include "key.h"
#include "tea5767.h"
#include "pt2314.h"
#include "UI.h"
#include "usart.h"

void init(){
	LCD_open();
	LCD_write("Bas:07  Tre:07  ",0,2);
	key_open(Hight);
	radio_open();
	pt2314_open(0);
}

void main(){
	unsigned char key_num=0xff;
	unsigned char mode ,old_mode;
	init();
	while(1){
		key_num = key_read();
		UI_IoCtrol(key_num);
		mode = UI_read((void *)0);
		
		if(mode != old_mode){
			UI_write(mode,1,0);
			old_mode = mode;
		}
		
	}
}