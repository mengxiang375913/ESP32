#ifndef _UI_H
#define _UI_H

#define BLUETOOTH 0xc4
#define FM  0xc3
#define AC 0xc1
#define USB 0xc2
#define SET 0xc5

void UI_write(unsigned char mode,int value,int len);
unsigned char UI_read(void *recover);
void UI_IoCtrol(int value);

#endif