#include "i2c.h"	  
#include "pt2314.h"
//代码已经经过多次修改
//能很好的支持CD3315&PT2315&TDA7315
//这里针对CD3314,其实只要改一改就好,通用的	   
//音量数据缓存.0,主音量,1,高音,2,低音,3,左声道,4,右声道,5,超重低音
//正点原子@SCUT
#define u8 unsigned char
u8 voltemp[6]={63,7,7,0,0,0};//声音寄存器,第一次刷机时的默认设置 
u8 INITDSP=0;  //0~4 eeprom级别 五种音效    
u8 Audio_Ch=1; //声道定义 默认MP3

//第31个字节用来保存 DSP应当调用的编号	   
#define DSPADDR    0   //DSP存放的地址区间首地址        0~30 	  
//保存DSP设置到eeprom
//保存区域：FM24C16的 0~30这段地址区间，总共30个字节
//地址：30用来保存INITDSP的值
//check ok 09/05/20
//void save_dsp(void)
//{	   
//	u8 t;
//	for(t=0;t<6;t++)
//		FM24C16_WriteOneByte(DSPADDR+6*INITDSP+t,voltemp[t]);//voltemp保存到INITDSP位置	 
//}   
//从eeprom调用DSP数据
//check ok 09/05/20
//void read_dsp(void)
//{
//	u8 t;	   
//	for(t=0;t<6;t++)
//		voltemp[t]=FM24C16_ReadOneByte(DSPADDR+6*INITDSP+t);//调用DSP[INITDSP]到voltemp	  	    
//} 
//主音量调节	  					 
//volume control code 00******   range 1~64
//num:1~64 
//check ok 09/05/19
void volume(u8 num) 
{			
	num=64-num;//转换为 0~63 				   
    IIC_Start();  
	IIC_WriteByte(0X88);//发送器件地址0X88,发送模式 
	IIC_testack();  								 
	IIC_WriteByte(num&0x3F);//设置音量 0~63
	IIC_testack();
	IIC_Stop();	 		  
}
//balance control code：10****** range 1~32
//用来调节左右声道的音量平衡  为独立调节 可以分别静音 调节范围1－－32
//wh：0 左声道，1 右声道 衰减最大为 31
//num:1~32
//check ok 09/05/19
void balance(u8 wh,u8 num)
{		
	//num=32-num;//转换为 0~31    
 	if(wh)num =0xe0;//左声道 111XXXXX
 	else num =0xc0; //右声道 110XXXXX
    IIC_Start();  
	IIC_WriteByte(0X88);//发送器件地址0X88,发送模式 
	IIC_testack();
	IIC_WriteByte(num);//发送数据		
	IIC_testack();
	IIC_Stop();

}
//声道和增益选择
//声道:0~3 
//增益:0~3 0增最大;3不变                    
//噪声抑制:0,开;1,关;
//check ok 09/05/19
void choosech(u8 ch,u8 gain,u8 loud)
{               
    u8 chtemp=0x40;
    chtemp+=gain<<3;
    chtemp+=loud<<2;
    chtemp+=ch;    				   		   
    IIC_Start();  
	IIC_WriteByte(0X88);//发送器件地址0X88,发送模式 
	IIC_testack();
	IIC_WriteByte(chtemp);
	IIC_testack();
    IIC_Stop();   
} 
//jud：0 低音调节 1 高音调节		  
//Bass 0110****    Treble 0111**** 
//num: 1~15
//check ok 09/05/19
void BassTreble(u8 jud,u8 num)
{ 			 
	//num=7; 
	if(num>7)num=22-num;  			   	  
    IIC_Start();  
	IIC_WriteByte(0X88);//发送器件地址0X88,发送模式 
	IIC_testack();	 			  
	if(jud)IIC_WriteByte(0x70|num);//0 低音调节 1 高音调节
	else IIC_WriteByte(0x60|num);
	IIC_testack();	
 	IIC_Stop();
}   	
		 
//音量初始化函数 
//check ok 09/05/19
//channal :0~4 4:静音
void pt2314_open(u8 channal)
{		
	Audio_Ch=channal;//记录当前路径 			      
    if(channal==4){volume(1);return;}//静音
	volume(voltemp[0]); //设置音量	
	 
	BassTreble(1,voltemp[1]);//高音设置   
	BassTreble(0,voltemp[2]);//低音设置  

  balance(0,voltemp[3]);//左通道音量最大
  balance(1,voltemp[4]);//右通道音量最大
	choosech(channal,3,voltemp[5]);//通道channal+1,增益0,超重开 
    
}

unsigned char pt2314_read(unsigned char num,unsigned char mod){
	if(mod == 0){
		if(num==1){
			return voltemp[2];
		}
		if(num==2){
			return voltemp[1];
		}
	}
}

void pt2314_write(unsigned char add_sub,unsigned char num,unsigned char mod){
	if(mod == 0){
		if(num==1){
			if(add_sub == 1){
				if(voltemp[2]<15)
					voltemp[2]++;
			}
			if(add_sub == 0){
				if(voltemp[2]>0)
				voltemp[2]--;
			}
			BassTreble(0,voltemp[2]);//低音设置 
		}
		if(num==2){
			if(add_sub == 1){
				if(voltemp[1]<15)
					voltemp[1]++;
			}
			if(add_sub == 0){
				if(voltemp[1]>0)
					voltemp[1]--;
			}
			BassTreble(1,voltemp[1]);//高音设置   
		}
	}
}

















