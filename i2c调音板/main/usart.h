#ifndef _USART_H
#define _USART_H
#define USART_COUNT 0x87
#define USART_CLOSE 0x88

#define FOSC    11059200L
#define BAUD    9600
#define CLOSE "!!!!"
#define OPEN "::::"
void Usart_write_Data(unsigned char dat);
void Usart_write(unsigned char *str,int length);
int Usart_read(unsigned char *rec,int length);
void Usart_open();
void Usart_ioctl(unsigned char *value,unsigned char cmd);
#endif