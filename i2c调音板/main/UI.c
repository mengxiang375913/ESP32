#include "UI.h"
#include "tea5767.h"
#include "PCF8574.h"
#include "pt2314.h"
#define sub1_key 6
#define auto_key 0
#define add10_key 2
#define add1_key 4
#define mod_key 7
#define bass_add_key auto_key
#define bass_sub_key add10_key
#define treb_add_key add1_key
#define treb_sub_key sub1_key
#define set_key	(mod_key+0x80)

unsigned char mode = AC;
unsigned char need_convert[5]={0,0,0,0,0};


/* unsigned char *num_to_str(unsigned long num){
	static unsigned char str[16]="Chandle:   .   ";
	str[0] = num/100000+'0';
	str[1] = num%100000/10000+'0';
	str[2] = num%10000/1000+'0';
	str[3]='.';
	str[4]=num%1000/100+'0';
	str[5]=num%100/10+'0';
	str[6]=num%10/1+'0';
	return str;
} */

unsigned char *UI_convert(long *num){   //单位转换函数
	if (mode==USB){

	}else if (mode==BLUETOOTH){

	}else if (mode==FM){
		static unsigned char str[11]="   .    ";
		str[0] = (*num)/100000+'0';
		str[1] = (*num)%100000/10000+'0';
		str[2] = (*num)%10000/1000+'0';
		str[3]='.';
		str[4]=(*num)%1000/100+'0';
		str[5]=(*num)%100/10+'0';
		str[6]=(*num)%10/1+'0';
		str[9] = '<';
		str[10] = '=';
		return str;
	}else if (mode==AC ){

	}else if (mode==SET){
		static unsigned char str[16]="Bas:00  Tre:00<=";
		//unsigned char num = pt2314_read(1,0);
		num[0] = num[0]%100;
		num[1] = num[1]%100;
		str[4] = num[0]/10 + '0';
		str[5] = num[0]%10 + '0';
		
		str[12] = num[1]/10 + '0';
		str[13] = num[1]%10 + '0';
		return str;
	}
	return "";
}

void UI_IoCtrol(int value){  //传入key值，为控制函数
	static old_mode;
	
	if (mode==USB){
		if (value == mod_key){
			mode = FM;
			return;
		}
		else if(value == set_key){
			old_mode = mode;
			mode = SET;			
			return;
		}
	}else if (mode==BLUETOOTH){
		if (value == mod_key){
				mode = AC;
				return;
			}
		else if(value == set_key){
			old_mode = mode;
			mode = SET;			
			return;
		}
	}else if (mode==FM){
		if (value == mod_key){
			mode = BLUETOOTH;
			return;
		}
		else if(value == auto_key){
			//tea767_auto_search(1);
			radio_write("",1,1);	need_convert[2] = 1;
			return;
		}
		else if(value == add10_key){
			radio_write("",1,0);	need_convert[2] = 1;
			return;
		}
		else if(value == add1_key){
			radio_write("",2,0);	need_convert[2] = 1;
			return;
		}
		else if(value == sub1_key){
			radio_write("",3,0);	need_convert[2] = 1;
			return;
		}
		else if(value == set_key){
			old_mode = mode;	need_convert[2] = 1;
			mode = SET;			
			return;
		}
	}else if (mode==AC ){
		if (value == mod_key){
			mode = USB;
			return;
		}
		else if(value == set_key){
			old_mode = mode;
			mode = SET;			
			return;
		}
	}else if (mode==SET){
		if (value==set_key){
			mode = old_mode;
			return ;
		}
		else if(value == bass_add_key){
			pt2314_write(1,1,0); need_convert[4] = 1;
			return ;
		}
		else if(value == bass_sub_key){
			pt2314_write(0,1,0); need_convert[4] = 1;
			return ;
		}
		else if(value == treb_add_key){
			pt2314_write(1,2,0); need_convert[4] = 1;
			return ;
		}
		else if(value == treb_sub_key){
			pt2314_write(0,2,0); need_convert[4] = 1;
			return ; 
		}
	}
}

void UI_write(unsigned char mode,int value,int len){ 	//由控制层直接可以写的函数
	if (mode==USB){
		LCD_write("USB           <=",0,1);
		LCD_write("  ",14,2);
		choosech(1,2,0);
	}else if (mode==BLUETOOTH){
		LCD_write("BT            <=",0,1);
		LCD_write("  ",14,2);
		choosech(3,0,0);
	}else if (mode==FM){
		LCD_write("FM            <=",0,1);
		LCD_write("  ",14,2);need_convert[2] = value;
		choosech(2,2,0);
	}else if (mode==AC ){
		LCD_write("AUX           <=",0,1);
		LCD_write("  ",14,2);
		choosech(0,0,0);
	}else if (mode==SET){
		LCD_write("  ",14,1);
		LCD_write("<=",14,2);need_convert[4] = value;
	}
}

unsigned char UI_read(void *recover){	//将当前模式代数返回给控制层
	if(need_convert[(mode - 0xc1)]>0){	
		if (mode==USB){
			//LCD_write("USB   ",7,1);
		}else if (mode==BLUETOOTH){
			//LCD_write("BLUE  ",7,1);
		}else if (mode==FM){
			long freq = 0;		
			freq = radio_read(0);	
			LCD_write(UI_convert(&freq),5,1);
		}else if (mode==AC ){
			//LCD_write("AUX   ",7,1);
		}		
		if(mode==SET){
			//LCD_write("SET ",7,1);
			long bass[2];
			bass[0] = pt2314_read(1,0);
			bass[1] = pt2314_read(2,0);
			LCD_write(UI_convert(bass),0,2);
		}
		need_convert[(mode - 0xc1)] = 0;
	}
	return mode;
}